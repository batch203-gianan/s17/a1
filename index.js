console.log ("Hello World");
/*
	1. Create a function which is able to prompt the user to provide their fullname, age, and location.
		-use prompt() and store the returned value into a function scoped variable within the function.
		-display the user's inputs in messages in the console.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	
	//first function here:
	function getUserDetails (){
		alert("Hi! Please Enter your Fullname, Age and Location Respectively");
		let userFullName = prompt("Please enter your full name :");
		let userAge = prompt ("Please enter your age:" );
		let userLocation = prompt ("Please enter your Location:" , "Look behind your back");
		console.log ("Hello, " + userFullName);
		console.log ("You are " + userAge + " years old.");
		console.log ("You live in " + userLocation);
	}
	getUserDetails();
/*
	2. Create a function which is able to print/display your top 5 favorite bands/musical artists.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
*/
	//second function here:
	function printFavoriteBands (){
		let band1 = prompt ("Enter your top five (5) favourite Bands:");
		let band2 = prompt ("Enter your top five (5) favourite Bands:");
		let band3 = prompt ("Enter your top five (5) favourite Bands:");
		let band4 = prompt ("Enter your top five (5) favourite Bands:");
		let band5 = prompt ("Enter your top five (5) favourite Bands:");
		console.log ("1. " + band1);
		console.log ("2. " + band2);
		console.log ("3. " + band3);
		console.log ("4. " + band4);
		console.log ("5. " + band5);
	}
	printFavoriteBands();
/*
	3. Create a function which is able to print/display your top 5 favorite movies of all time and show Rotten Tomatoes rating.
		-Look up the Rotten Tomatoes rating of your favorite movies and display it along with the title of your favorite movie.
		-invoke the function to display your information in the console.
		-follow the naming conventions for functions.
	
*/
	//third function here:
	function printFavoriteMovies() {
		alert ("Kindly input your top five (5) favourite movies and I will rate them!");
		let movie1 = prompt("Enter Movie #1");
		console.log ("1. " + movie1);
		console.log ("Rotten tomatoes rating for " + movie1 + " is 25%");

		let movie2 = prompt("Enter Movie #2");
		console.log ("2. " + movie2);
		console.log ("Rotten tomatoes rating for " + movie2 + " is 55%");

		let movie3 = prompt("Enter Movie #3");
		console.log ("3. " + movie3);
		console.log ("Rotten tomatoes rating for " + movie3 + " is 33%");

		let movie4 = prompt("Enter Movie #4");
		console.log ("4. " + movie4);
		console.log ("Rotten tomatoes rating for " + movie4 + " is 97%");

		let movie5 = prompt("Enter Movie #5");
		console.log ("5. " + movie5);
		console.log ("Rotten tomatoes rating for " + movie5 + " is 81%");				
	}
	printFavoriteMovies();

/*
	4. Debugging Practice - Debug the following codes and functions to avoid errors.
		-check the variable names
		-check the variable scope
		-check function invocation/declaration
		-comment out unusable codes.
*/

let printUsers = function printUsers(){
	alert("Hi! Please add the names of your friends.");
	let friend1 = prompt("Enter your first friend's name:"); 
	let friend2 = prompt("Enter your second friend's name:"); 
	let friend3 = prompt("Enter your third friend's name:");

	console.log("You are friends with:")
	console.log(friend1); 
	console.log(friend2); 
	console.log(friend3); 
};
printUsers();

// console.log(friend1);
// console.log(friend2);